# gnuplot-iostream
add_library(gnuplot-iostream INTERFACE)
target_include_directories(gnuplot-iostream INTERFACE "gnuplot-iostream")
target_link_libraries(gnuplot-iostream INTERFACE Boost::iostreams Boost::system Boost::filesystem)

# yaml-cpp
add_subdirectory(yaml-cpp)