#include <iostream>

#include "yaml-cpp/yaml.h"

int main(int argc, char * argv[])
{
	std::string filename = argv[1];

	std::cout << "Reading " << filename << std::endl;

	YAML::Node config = YAML::LoadFile(filename);

	const std::string username = config["username"].as<std::string>();
	const std::string password = config["password"].as<std::string>();

	std::cout << username << " " << password << std::endl;

	return 0;
}