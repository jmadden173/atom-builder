# Presentation Nodes

## Title Slide

Hello everyone, I'm John, and I am going to talk about how varrying the input sequence of atoms into a 2D system affects the resulting structure.

## Introduction

My inspiration for this topic came primary from two sources. The first shown on the left is the video that was shown during lecture simulating *Crystal Growth by Molecular Beam Epitaxy*. On the right there is a screenshot from Lecture 5 that shows a simulation that Professor Nobby preformed on a similar topic. I heavily based my topic off this, but controlled the parameters myself.

## Approach

My approach to simulating the system has 5 requirements.

1. Fixed size system in 2D. I wanted a solid starting point that would be easy to implement and can be expanded on later if time allowed..
2. Method for varrying the input sequence of atoms to the system.
3. Have a distinct substrate layer that the starting sequence of atoms have a place to bond to.
4. Visually represent the system. There is no point in simulating the system if I am not able to generate some data from it.
5. Track the energy of the system. As the energy influences where atoms settle into place, I wanted to track the total energy to observe the relationship.

## Reading Plots

Here are common plots that are going to be shown throughout the presentation. On the left hand side there is a heatmap of the system. The substrate is not shown as it is position at y = -1. The interface energy of each substrate atom was fixed at 1. The colorbar next to the heatmap allows differentiating of atom types.

On the right there is a plot of the energy of the system vs the number of atoms in the system. It starts at a non-zero value as the substrate has surface energy associated with it.

## Sequence Types

There was two types of sequences examined that I have denoted: *simple*, and *rotating*. They both share a list of atoms that then is combined with parameters to generated unique sequences.

- The rotating sequence has one parameter. Number of Iterations. The sequence goes through the list of atoms repeating until the number of iterations is reached.
- The simple sequence has two parameters. Number of Repetitions, and Number of iterations. The number of repetitions is how many times an atom is repeated before moving into the next atom. The number of iterations is the same as in the rotating sequence.

> Thinking about it now the simple rotating sequence is a subset of the simple sequence.

## Generated Systems

### Small System

This slide shows the results obtained from simulating a small system, defined as 10 unit by 10 unit with 50 atoms total. The sequence of atoms added to the system by their interface energies were: 40, 45, 50, 55, 60. For the simple the number of repetitions was 5. This was held constant throughout the different sizes of simulations.

The rotating system produced a very uniform result with columns for each of the atom types. This is because there is no interface energy between atoms of the same type. Thus the atoms added are most likely to settle into a spot next to an atom of the same type. The substrate interface energy is far lower than any of the interface energies of the atoms resulting in Volmer-Webber growth.

The simple system resulted in Frank van der Merwe growth. Both total energies are similar.

### Medium System

For the medium system, the size of the system was increased to 20 by 20 and a total of 250 atoms were added.

The medium system reflects the same structure observed on the small system, although the total energies differ by approximatly 600 units. This is due to the rotating system growing vertical until reaching the vertical limit. The system started expaning in the positive x direction to minimize its energy. This explains the increased energy over the simple system as more surfaces are exposed and it is not making full use of the substrate.

### Large System

For the large system, the size was increased to 50 by 50 and a total of 1000 atoms were added.

The large rotating system exhibits a more extreme case of the medium system as it covered two faces of the border. The energy difference between the rotating and simple systems increased to approximatly 17000!

The simple system exhibited similar aspects to the small rotating system with vertical columns of same type atoms. This could be due to the greater amount of atoms "smoothing" out the difference between input sequences resulting in similar structures. Think law of large numbers.

## Further Ideas

In the process of the project there were a few aspects I would have liked to expand on.

- First is a system of non static dimensions. This would allow the structure to grow in whichever direction to minimize the energy.
- The most complicated programically, would be adding another dimension to the simulation.
- The code was written to allow for variable sequences, but only two varriations were explored in this presentation. I would want to see a wider varriety of sequences simulated.
- Only the mean interface energy was used in the simulation. The code is there for simulation where the interface energy is greater than or less than the surface energies.
- The substrate was held constant for all the simulations. It could have an influence on the way the structure grows as observed in the rotating systems.

## References

In my last slide are references that were discussed in the presentation. I would like to thank Professor Nobby for teaching the course and peaking my interest in many different disciplines of material science regarles if I understood them or not. Thank you for your attention.