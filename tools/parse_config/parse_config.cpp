#include <iostream>

#include "system_config.hpp"

int main(int argc, char * argv[])
{
	std::string filename = argv[1];

	std::cout << "Reading " << filename << std::endl;

	atom_builder::config config(filename);
	std::vector<atom_builder::system_config>::iterator it_config;
	for (it_config = config.system_configs.begin(); it_config != config.system_configs.end(); ++it_config)
	{
		std::cout << "name: " << it_config->get_name() << ", ";
		std::cout << "size: (" << it_config->get_size_x() << ", " << it_config->get_size_y() << ")" << ", ";
		std::cout << "type: " << it_config->get_type() << ", ";
		std::cout << "sequence: ";
		atom_builder::system_config::iterator it_atom(*it_config);
		for (it_atom = it_config->begin(); it_atom != it_config->end(); ++it_atom)
		{
			std::cout << it_atom->get_sigma_empty() << " ";
		}
		std::cout << std::endl;
	}

	return 0;
}