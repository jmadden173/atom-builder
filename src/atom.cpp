#include "atom.hpp"

atom_builder::atom::atom(int _sigma)
{
    // Set sigma
    m_sigma = _sigma;
}

bool atom_builder::operator==(const atom &lhs, const atom &rhs)
{
    // Self guard
    if (&lhs == &rhs)
        return true;

    return (lhs.m_sigma == rhs.m_sigma);
}

int atom_builder::get_sigma_high(const atom &atom1, const atom &atom2)
{
    // No interface energy between same atoms
    if (atom1 == atom2)
        return 0;

    // Sum
    int sigma;
    sigma = atom1.get_sigma_empty() + atom2.get_sigma_empty();

    return sigma;
}

int atom_builder::get_sigma_mean(const atom &atom1, const atom &atom2)
{
    // No interface energy between same atoms
    if (atom1 == atom2)
        return 0;

    // Get average
    int sigma;
    sigma = atom1.get_sigma_empty() + atom2.get_sigma_empty();
    sigma /= 2;

    return sigma;
}

int atom_builder::get_sigma_low(const atom &atom1, const atom &atom2)
{
    // No interface energy between same atoms
    if (atom1 == atom2)
        return 0;

    // Get difference
    int sigma;
    sigma = atom1.get_sigma_empty() - atom2.get_sigma_empty();
    sigma = abs(sigma);

    return sigma;
}