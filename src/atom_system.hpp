#ifndef ATOM_SYSTEM_HPP
#define ATOM_SYSTEM_HPP

#include <list>
#include <vector>
#include <stdexcept>
#include <cmath>
#include <numeric>
#include <iterator>
#include <utility>
#include <random>

#include "atom.hpp"
#include "vector2d.hpp"
#include "system_config.hpp"

namespace atom_builder
{
    class atom_system
    {
    public:
        /**
         * @brief Construct a new atom system object
         * 
         * The coordinate system starts at (0, 0). The substrate layer (y = -1)
         * is created using _substrate_atom.
         * 
         * @param _substrate_atom 
         * @param _system_config 
         */
        atom_system(atom _substrate_atom, system_config _system_config);

        /**
         * @brief Destroy the atom_system object.
         *
         * Currently does nothing.
         */
        ~atom_system();

        /**
         * Places an atom in the system at a forced position. The energy of the
         * system is updated based on position placed.
         *
         * @param _atom New atom.
         * @param pos Postion of atom.
         */
        void place_atom(atom _atom, const vector2d &pos);

        /**
         * @brief Adds a now atom to the system.
         *
         * Places the atom in the spot to minimize the interface energy.
         *
         * @param _atom atom to place.
         * @return Position of placed atom.
         */
        vector2d add_atom(atom _atom);

        /**
         * @brief Adds a sequence of atoms from system config
         * 
         * @param _system_config 
         */
        void add_atoms(system_config _system_config);

        /**
         * @brief Get the total energy of the system.
         *
         * @return System energy.
         */
        int get_energy(void);

        /**
         * @brief Get the size of the x dimension.
         *
         * @return Size of x dimension.
         */
        int get_size_x(void) const { return size_x; }

        /**
         * @brief Get the size of the y dimension.
         *
         * @return Size of y dimension.
         */
        int get_size_y(void) const { return size_y; }

        /**
         * Get initial energy of the system. Initial energy is accumulated every
         * time atom is placed.
         * 
         * @return Const reference to initial energy
         */
        const int & initial_energy(void) const { return m_initial_energy; }

        /**
         * Get atoms and their position in the system. Substrate atoms are not
         * included. Atoms outside the bounds of the system are not included.
         * 
         * @return List of atoms and their position
         */
        std::list<std::pair<atom, vector2d>> atoms(void) const;

        /**
         * Get all atoms and their position in the system. This can include
         * atoms that are out of bounds such as substrate atoms.
         * 
         * @return List of pair of atoms and their position
         */
        std::list<std::pair<atom, vector2d>> all_atoms(void) const;

    private:
        int size_x;
        int size_y;

        /**
         * Initial energy of the system. Accumulated whenever an atom is placed.
         * 
         * @see place_atom
         */
        int m_initial_energy = 0;

        /*
         * Memory list of the change of energies.
         */
        std::list<int> delta_energies;

        /**
         * List of atoms in the system not including substrate atoms.
         * 
         */
        std::list<std::pair<atom, vector2d>> atom_list;

        /**
         * List of substrate atoms.
         */
        std::list<std::pair<atom, vector2d>> m_substrate_list;

        /**
         * @brief Find positoins that yield the lowest overall system energy.
         *
         * The position is calculated by finding the change of energy at each
         * position and selecting the position that has the smallest change in
         * energy.
         *
         * @param _atom Atom
         * @param positions Available positions.
         * @return List of positions sharing the lowest energy state.
         */
        std::list<vector2d> find_lowest_energy_positions(const atom &_atom, std::list<vector2d> positions);

        /**
         * @brief Calculates the energy difference of adding atom surrounded by
         * adj_atoms to the system.
         *
         * Interface energy is calculated by finding how many sides of atom are
         * occupied by adjacent atoms. The energy difference is calculated with
         * the surface energy and interface energy.
         *
         * @param _atom Central Atom.
         * @param adj_atoms List of atoms surrounding the central.
         * @return Energy difference of adding atom.
         */
        int calc_energy_diff(const atom &_atom, std::list<atom> adj_atoms);

        /**
         * @brief Finds available positions for a atom in the system.
         *
         * Valid positions include:
         *  - On the x plane on the substrate.
         *  - Adjacent to any other atoms in the system.
         *  - Positions that are not occupied by other atoms.
         *
         * @return List of available and valid positions.
         */
        std::list<vector2d> find_available_positions(void);

        /**
         * @brief Finds the adjacent positions to origin.
         *
         * The list in the order of North, East, South, West. Returning a
         * unordered list was purposefully done to prevent using this property.
         *
         * @param origin Position to be treated as (0, 0).
         * @return List of adjacent positions.
         */
        std::list<vector2d> find_adj_pos(const vector2d &origin);

        /**
         * @brief Finds the adjacent atoms to the origin
         *
         * Does not guarantee that the list of positions reflect their positions
         * relative to the origin.
         *
         * @param origin Position treated as (0, 0)
         * @return List of adjacent atoms.
         */
        std::list<atom> find_adj_atoms(const vector2d &origin);

        /**
         * @brief Add substrate atoms to system
         * 
         * Substrate is the 0th level of the system spanning the entire x axis.
         * 
         * @param _atom Substrate atom
         */
        void add_substrate(atom _atom);

        /**
         * @brief Checks if a position is out of bounds in the system.
         *
         * @param pos Position.
         * @return true if out of bounds. false otherwise.
         */
        bool is_out_of_bounds(const vector2d &pos) const;

        /**
         * @brief Checks if a positions is occupied by a atom already in the
         * system.
         *
         * @param pos Position.
         * @return true if position is occupied. false otherwise.
         */
        bool is_occupied(const vector2d &pos);

        /**
         * @brief Update the list of delta energies with new delta energy
         *
         * @param delta_energy Change in energy.
         */
        void update_delta_energies(int delta_energy);

        /**
         * @brief Selects a random position from a list of positions.
         *
         * Uses uniform distribution to weight each of the positions the same.
         *
         * @param positions List of positions to select from.
         * @return Random position.
         */
        vector2d random_position(std::list<vector2d> positions);

        /**
         * @brief Get the atom at position.
         *
         * @param pos Position of atom.
         * @return atom
         */
        atom get_atom(const vector2d &pos);

        friend void plot_energy(const atom_system &);
    };
}

#endif // ATOM_SYSTEM_HPP