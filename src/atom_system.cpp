#include "atom_system.hpp"

atom_builder::atom_system::atom_system(atom _substrate_atom, system_config _system_config)
{
    size_x = _system_config.get_size_x();
    size_y = _system_config.get_size_y();

    // Substrate
    add_substrate(_substrate_atom);

    // Add atoms
    add_atoms(_system_config);
}

atom_builder::atom_system::~atom_system(void) {}

void atom_builder::atom_system::place_atom(atom _atom, const vector2d &pos)
{
    if (is_occupied(pos))
    {
        throw std::invalid_argument("Atom already exists in position!");
    }

    // Add energy to the atom_system
    std::list<atom> adj_atoms = find_adj_atoms(pos);
    int delta_energy = calc_energy_diff(_atom, adj_atoms);
    update_delta_energies(delta_energy);

    // Add atom to the list
    atom_list.emplace_back(_atom, pos);
}

atom_builder::vector2d atom_builder::atom_system::add_atom(atom _atom)
{
    // Find available positions
    std::list<vector2d> positions_avail;
    positions_avail = find_available_positions();

    // Find best positions
    std::list<vector2d> lowest_energy_positions = find_lowest_energy_positions(_atom, positions_avail);

    // Select random positions
    vector2d pos = random_position(lowest_energy_positions);

    // Place atom
    place_atom(_atom, pos);

    return pos;
}

void atom_builder::atom_system::add_atoms(system_config _system_config)
{
	system_config::iterator it_atom(_system_config);
    for (it_atom = _system_config.begin(); it_atom != _system_config.end(); ++it_atom)
    {
        add_atom(*it_atom);
    }
}

int atom_builder::atom_system::get_energy(void)
{
    int energy = m_initial_energy;
    energy += accumulate(delta_energies.begin(), delta_energies.end(), 0);
    return energy;
}

std::list<std::pair<atom_builder::atom, atom_builder::vector2d>> atom_builder::atom_system::atoms(void) const
{
    // Duplicate list
    std::list<std::pair<atom, vector2d>> bounded_atom_list;
    bounded_atom_list = atom_list;

    // Remove if out of bounds of the atom_system
    bounded_atom_list.remove_if([this](const std::pair<atom, vector2d> & atom_pair)
                        { return is_out_of_bounds(atom_pair.second); });

    return bounded_atom_list;
}

std::list<std::pair<atom_builder::atom, atom_builder::vector2d>> atom_builder::atom_system::all_atoms(void) const
{
    std::list<std::pair<atom_builder::atom, atom_builder::vector2d>> all;
    all.insert(all.begin(), atom_list.begin(), atom_list.end());
    all.insert(all.begin(), m_substrate_list.begin(), m_substrate_list.end());

    return all;
}

std::list<atom_builder::vector2d> atom_builder::atom_system::find_lowest_energy_positions(const atom &_atom, std::list<vector2d> positions)
{
    // Throw if positions argument is emtpy
    if (positions.empty())
    {
        throw std::invalid_argument("Positions is empty.");
    }

    // List of the positions and corresponding change of energy
    std::list<std::pair<vector2d, int>> energies;

    // Loop over possible positions
    std::list<vector2d>::const_iterator pos;
    for (pos = positions.begin(); pos != positions.end(); ++pos)
    {
        // Find adjacent atoms
        std::list<atom> adj_atoms;
        adj_atoms = find_adj_atoms(*pos);

        // Calculate interface energy
        int energy;
        energy = calc_energy_diff(_atom, adj_atoms);

        // Add to list
        energies.push_back(std::make_pair(*pos, energy));
    }

    // Throw if no positions are found
    if (energies.empty())
    {
        throw std::length_error("No positions found.");
    }

    std::list<vector2d> lowest_energy_positions;

    std::list<std::pair<vector2d, int>>::const_iterator iter_energies = energies.begin();

    // Grab first energy
    int lowest_energy = iter_energies->second;
    lowest_energy_positions.push_back(iter_energies->first);
    ++iter_energies;

    for (iter_energies; iter_energies != energies.end(); ++iter_energies)
    {
        if (iter_energies->second < lowest_energy)
        {
            lowest_energy = iter_energies->second;
            lowest_energy_positions.clear();
            lowest_energy_positions.push_back(iter_energies->first);
        }
        else if (iter_energies->second == lowest_energy)
        {
            lowest_energy_positions.push_back(iter_energies->first);
        }
    }

    return lowest_energy_positions;
}

std::list<atom_builder::atom> atom_builder::atom_system::find_adj_atoms(const vector2d &pos)
{
    std::list<atom> adj_atoms;

    // Loop over adjacent positions
    std::list<vector2d> adj_pos = find_adj_pos(pos);
    std::list<vector2d>::const_iterator it_adj;
    for (it_adj = adj_pos.begin(); it_adj != adj_pos.end(); ++it_adj)
    {
        // Add atom if exists
        if (is_occupied(*it_adj))
            adj_atoms.push_back(get_atom(*it_adj));
    }

    return adj_atoms;
}

int atom_builder::atom_system::calc_energy_diff(const atom &_atom, std::list<atom> adj_atoms)
{
    // Check for invalid adj_atom size
    if (adj_atoms.size() > 4)
    {
        throw std::length_error("More than 4 elements in adj_atoms");
    }

    // Energy difference
    int energy = 0;

    // Add surface energy of empty sides
    energy += (4 * _atom.get_sigma_empty());

    // Remove surface energy of sides occupied
    std::list<atom>::const_iterator atom_iter;
    for (atom_iter = adj_atoms.begin(); atom_iter != adj_atoms.end(); ++atom_iter)
    {
        // Remove surface energy of each side
        energy -= (_atom.get_sigma_empty() + atom_iter->get_sigma_empty());
        // Add interface energy between atoms
        energy += get_sigma_mean(_atom, *atom_iter);
    }

    return energy;
}

std::list<atom_builder::vector2d> atom_builder::atom_system::find_available_positions(void)
{
    std::list<std::pair<atom, vector2d>> all = all_atoms();

    std::list<vector2d> positions;

    // Add positions adjacent to existing atoms
    std::list<std::pair<atom, vector2d>>::const_iterator iter_atom;
    for (iter_atom = all.begin(); iter_atom != all.end(); ++iter_atom)
    {
        std::list<vector2d> adj = find_adj_pos(iter_atom->second);
        positions.splice(positions.end(), adj);
    }

    // Keep unique values
    positions.sort();
    positions.unique();

    // Remove if out of bounds of the atom_system
    positions.remove_if([this](const vector2d &pos)
                        { return is_out_of_bounds(pos); });

    // Remove adjacent positions that already contain atoms
    for (iter_atom = all.begin(); iter_atom != all.end(); ++iter_atom)
    {
        positions.remove(iter_atom->second);
    }

    return positions;
}

std::list<atom_builder::vector2d> atom_builder::atom_system::find_adj_pos(const vector2d &origin)
{
    std::list<vector2d> adj;

    adj.push_back(origin + vector2d(0, 1));
    adj.push_back(origin + vector2d(0, -1));
    adj.push_back(origin + vector2d(1, 0));
    adj.push_back(origin + vector2d(-1, 0));

    return adj;
}

void atom_builder::atom_system::add_substrate(atom _atom)
{
    // Place atoms
    for (int x = 0; x < get_size_x(); ++x)
    {
        vector2d pos(x, -1);

        // Add energy to the atom_system
        std::list<atom> adj_atoms = find_adj_atoms(pos);
        m_initial_energy += calc_energy_diff(_atom, adj_atoms);
    
        m_substrate_list.emplace_back(_atom, pos);
    }
}

bool atom_builder::atom_system::is_out_of_bounds(const vector2d &pos) const
{
    // X plane
    if ((0 > pos.x) || (pos.x >= this->size_x))
    {
        return true;
    }
    // Y plane
    else if ((0 > pos.y) || (pos.y >= this->size_y))
    {
        return true;
    }

    return false;
}

bool atom_builder::atom_system::is_occupied(const vector2d &pos)
{
    // TODO Convert to binary search

    std::list<std::pair<atom, vector2d>> all = all_atoms();

    std::list<std::pair<atom, vector2d>>::const_iterator iter_atom;
    for (iter_atom = all.begin(); iter_atom != all.end(); ++iter_atom)
    {
        if (iter_atom->second == pos)
            return true;
    }

    return false;
}

void atom_builder::atom_system::update_delta_energies(int delta_energy)
{
    delta_energies.push_back(delta_energy);
}

atom_builder::vector2d atom_builder::atom_system::random_position(std::list<vector2d> positions)
{
    // Uniform random number generator
    static std::random_device rd;
    static std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, distance(positions.begin(), positions.end()) - 1);

    // Advance iterator random amount
    std::list<vector2d>::const_iterator it = positions.begin();
    advance(it, dis(gen));

    return *it;
}

atom_builder::atom atom_builder::atom_system::get_atom(const vector2d & pos)
{
    std::list<std::pair<atom, vector2d>> all = all_atoms();

    std::list<std::pair<atom, vector2d>>::const_iterator it;
    for (it = all.begin(); it != all.end(); ++it)
    {
        if (it->second == pos)
            return it->first;
    }

    throw std::invalid_argument("No atom at position");
}