#ifndef VECTOR2D_HPP
#define VECTOR2D_HPP

#include <cmath>

namespace atom_builder
{
    class vector2d
    {
    public:
        /** X position */
        int x;

        /** Y position */
        int y;

        /**
         * @brief Construct a new vector2d object at (0, 0).
         *
         *
         */
        vector2d(void);

        /**
         * @brief Construct a new vector2d object at (x, y).
         *
         * @param _x X value.
         * @param _y Y value.
         */
        vector2d(int _x, int _y);

        /**
         * @brief Overloaded copy assignment.
         *
         * Copies the x and y coordinate from other vector2d to this.
         *
         * @param other Other vector2d.
         * @return This vector2d.
         */
        vector2d &operator=(const vector2d &other);

        /**
         * @brief Get the magnitude of the vector
         * 
         * Uses the equation sqrt(x^2 + y^2).
         * 
         * @return Magnitude of vector
         */
        double mag(void) const { return sqrt(pow(x, 2) + pow(y, 2)); }
    };

    /**
     * @brief Equals comparison operator.
     *
     * Vectors are equal if both x and y are equal.
     *
     * @param lhs Left hand size vector2d.
     * @param rhs Right hand size vector2d.
     * @return true if equal
     * @return false otherwise
     */
    bool operator==(const vector2d &lhs, const vector2d &rhs);

    /**
     * @brief Overloaded add operator.
     *
     * Add a vector in the form (x1 + x2, y1 + y2).
     *
     * @param other Second vector to add (x2, y2).
     * @return Reference to the added vector.
     */
    vector2d operator+(const vector2d &lhs, const vector2d &rhs);

    /**
     * @brief Overloaded less than operator.
     *
     * Both x and y on the lhs are checked to be less the right hand side.
     *
     * @param lhs Left hand side.
     * @param rhs Right hand side.
     * @return true if lhs x and y are both greater than rhs, false otherwise.
     */
    bool operator<(const vector2d &lhs, const vector2d &rhs);

    /**
     * @brief Overloaded greater than operator.
     *
     * @param lhs Left hand side.
     * @param rhs Right hand side.
     * @return true if lhs x and y are both less than rhs, false otherwise.
     */
    inline bool operator>(const vector2d &lhs, const vector2d &rhs) { return rhs < lhs; }

    /**
     * @brief Overloaded less then equal operator.
     *
     * @param lhs Left hand side.
     * @param rhs Right hand side.
     * @return true if lhs x and y are both less than or equal to rhs, false othersize.
     */
    inline bool operator<=(const vector2d &lhs, const vector2d &rhs) { return !(lhs > rhs); }

    /**
     * @brief Overloaded greater then equal operator.
     *
     * @param lhs Left hand side.
     * @param rhs Right hand side.
     * @return true if lhs x and y are both greater than or equal to rhs, false othersize.
     */
    inline bool operator>=(const vector2d &lhs, const vector2d &rhs) { return !(lhs < rhs); }
}

#endif // VECTOR2D_HPP