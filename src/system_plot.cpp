#include "system_plot.hpp"

void atom_builder::plot_heatmap(const atom_system & sys)
{
    // Create NaN matrix
    std::vector<std::vector<double>> image;
    for (int i = 0; i < sys.get_size_x(); ++i)
    {
        std::vector<double> row;
        for (int j = 0; j < sys.get_size_y(); ++j)
        {
            row.push_back(NAN);
        }
        image.push_back(row);
    }

    std::list<std::pair<atom, vector2d>> atoms;
    atoms = sys.atoms();

    // Fill in with positions of atoms
    std::list<std::pair<atom, vector2d>>::const_iterator it_atom;
    for (it_atom = atoms.begin(); it_atom != atoms.end(); ++it_atom)
    {
        image[it_atom->second.y][it_atom->second.x] = it_atom->first.get_sigma_empty();
    }

    std::pair<float, float> xrange = get_xrange(sys);
    gp_heatmap << "set xrange [" << xrange.first << ":" << xrange.second << "]" << std::endl;
    std::pair<float, float> yrange = get_yrange(sys);
    gp_heatmap << "set yrange [" << yrange.first << ":" << yrange.second << "]" << std::endl;

    gp_heatmap << "set key off" << std::endl;

    gp_heatmap << "set datafile missing 'nan'" << std::endl;

    gp_heatmap << "plot '-' matrix with image" << std::endl;

    gp_heatmap.send1d(image);

    gp_heatmap << "reset" << std::endl;
}

void atom_builder::save_heatmap(const atom_system & sys, std::string filename)
{
    // Set output
    gp_heatmap << "set term push" << std::endl;
    gp_heatmap << "set term png" << std::endl;
    gp_heatmap << "set output '" << filename << "'" << std::endl;

    plot_heatmap(sys);

    // Reset
    gp_heatmap << "set term pop" << std::endl;
    gp_heatmap << "set output" << std::endl;
}

void atom_builder::plot_energy(const atom_system & sys)
{
    std::vector<std::pair<int, int>> pts;

    int num_atoms = 0;
    int total_energy = sys.initial_energy();

    // Initial energy
    pts.emplace_back(num_atoms, total_energy);

    // Generate points
    std::list<int>::const_iterator it;
    for (it = sys.delta_energies.begin(); it != sys.delta_energies.end(); ++it)
    {
        ++num_atoms;
        total_energy += *it;
        pts.emplace_back(num_atoms, total_energy);
    }

    gp_energy << "set title 'Total energy vs Number of atoms" << std::endl;
    gp_energy << "set ylabel 'Total Energy [J]'" << std::endl;
    gp_energy << "set xlabel 'Number of atoms'" << std::endl;

    gp_energy << "set key off" << std::endl;

    gp_energy << "plot '-' with lines" << std::endl;

    gp_energy.send1d(pts);

    gp_energy << "reset" << std::endl;
}

void atom_builder::save_energy(const atom_system & sys, std::string filename)
{
    // Set output
    gp_energy << "set term push" << std::endl;
    gp_energy << "set term png" << std::endl;
    gp_energy << "set output '" << filename << "'" << std::endl;

    plot_energy(sys);

    // Reset
    gp_energy << "set term pop" << std::endl;
    gp_energy << "set output" << std::endl;
}
	
std::pair<float, float> atom_builder::get_xrange(atom_system sys)
{
    std::pair<float, float> xrange;
    xrange.first = -0.5f;
    xrange.second = (float) sys.get_size_x() - 0.5;

    return xrange;
}

std::pair<float, float> atom_builder::get_yrange(atom_system sys)
{
    std::pair<float, float> yrange;
    yrange.first = -0.5f;
    yrange.second = (float) sys.get_size_y() - 0.5;

    return yrange;
}