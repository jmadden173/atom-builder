#ifndef ATOM_HPP
#define ATOM_HPP

#include <cmath>

#include "vector2d.hpp"

namespace atom_builder
{
    class atom
    {
    public:
        /**
         * @brief Construct a new atom object.
         *
         * Defines a new atom with a set interface energy.
         *
         * @param _sigma Interface energy.
         */
        atom(int _sigma);

        /**
         * @brief Get the interface energy.
         *
         * The interface energy of the atom.
         *
         * @return Interface energy.
         */
        int get_sigma_empty(void) const { return m_sigma; };

        /**
         * @brief Boolean comparison for atom.
         *
         * Checks if sigma of the left hand side and right hand side of the operator.
         *
         * @param lhs Left hand side.
         * @param rhs Right hand side.
         * @return true if interface energies, false otherwise
         */
        friend bool operator==(const atom &lhs, const atom &rhs);

    private:
        int m_sigma;
    };

    /**
     * @brief Get the high interface energy between two atoms.
     *
     * Interface energy is higher than the surface energy of either atoms.
     *
     * @param atom Other atom.
     * @return Interface energy.
     */
    int get_sigma_high(const atom &atom1, const atom &atom2);

    /**
     * @brief Get the mean interface energy between two atoms.
     *
     * Interface energy is between the surface energy of either atoms.
     *
     * @param atom Other atom.
     * @return Interface energy.
     */
    int get_sigma_mean(const atom &atom1, atom const &atom2);

    /**
     * @brief Get the low interface energy between two atoms.
     *
     * Interface energy is lower than the surface energy of either atoms.
     *
     * @param atom1 First atom.
     * @param atom2 Second atom.
     * @return Interface energy.
     */
    int get_sigma_low(const atom &atom1, atom const &atom2);
}

#endif // ATOM_HPP