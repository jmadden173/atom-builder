#ifndef SYSTEM_CONFIG_H
#define SYSTEM_CONFIG_H

#include <iostream>
#include <chrono>
#include <stdexcept>
#include <string>
#include <vector>
#include <iterator>
#include <cstddef>

#include "yaml-cpp/yaml.h"

#include "atom.hpp"

namespace atom_builder
{
	class system_config
	{
	public:
		/**
		 * @brief Construct a new system config object
		 *
		 */
		system_config(void);

		/**
		 * @brief Set the name
		 * 
		 * @param _name New name
		 */
		void set_name(std::string _name) { m_name = _name; }

		/**
		 * @brief Get the name
		 * 
		 * @return name string
		 */
		std::string get_name(void) const { return m_name; }

		/**
		 * @brief Set the type
		 * 
		 * @param _type New type
		 */
		void set_type(std::string _type) { m_type = _type; }

		/**
		 * @brief Get the type
		 * 
		 * @return name string
		 */
		std::string get_type(void) const { return m_type; }

		/**
		 * @brief Set the iterations
		 * 
		 * @param _iterations New iterations
		 */
		void set_iterations(unsigned int _iterations) { m_iterations = _iterations; }

		/**
		 * @brief Get the iterations
		 * 
		 * @return Number of iterations
		 */
		unsigned int get_iterations(void) const { return m_iterations; }

		/**
		 * @brief Set the repetitions
		 * 
		 * @param _reptitions New reptitions
		 */
		void set_repetitions(unsigned int _reptitions) { m_repetitions = _reptitions; }

		/**
		 * @brief Get the repetitions
		 * 
		 * @return Number of repetitions
		 */
		unsigned int get_repetitions(void) const { return m_repetitions; }

		/**
		 * @brief Get reference to the atom sequence
		 * 
		 * @return Reference to vector of atoms
		 */
		std::vector<atom> & get_atoms(void) { return m_atoms; }

		/**
		 * @brief Set the size of the system in the x direction
		 * 
		 * @param _size_x New size
		 */
		void set_size_x(int _size_x) { m_size_x = _size_x; }

		/**
		 * @brief Get the size of the system in the x direction
		 * 
		 * @return Size in the x direction
		 */
		int get_size_x(void) const { return m_size_x; };
		
		/**
		 * @brief Set the size of the system in the y direction
		 * 
		 * @param _size_y New size
		 */
		void set_size_y(int _size_y) { m_size_y = _size_y; }

		/**
		 * @brief Get the size of the system in the y direction
		 * 
		 * @return Size in the y direction
		 */
		int get_size_y(void) const { return m_size_y; };

		/**
		 * @brief operator<< overload
		 * 
		 * Inserts description of system_config to output stream.
		 *
		 * @param os Output stream
		 * @param obj System_config object
		 * @return Output stream
		 */
		friend std::ostream & operator<<(std::ostream &os, const system_config & obj);

		struct iterator
		{
			using iterator_category = std::input_iterator_tag;
			using value_type = std::vector<atom>::value_type;
			using pointer = std::vector<atom>::pointer;
			using reference = std::vector<atom>::reference;

			/**
			 * @brief Construct a new iterator object
			 * 
			 * @param _system_config Pointer to parent object
			 */
			iterator(system_config & _system_config) : m_system_config(_system_config) {}

			/**
			 * @brief Construct a new iterator object with a default iterator
			 * 
			 * @param _it Atom vector iterator
			 * @param _system_config Pointer to parent object
			 */
			iterator(std::vector<atom>::iterator _it, system_config & _system_config) : m_it(_it), m_system_config(_system_config) {}

			iterator & operator=(const iterator & other)
			{
				// Self guard
				if (this == &other)
					return *this;

				m_it = other.m_it;
				m_system_config = other.m_system_config;

				return *this;	
			}

			reference operator*() const { return m_it.operator*(); }
			pointer operator->() { return m_it.operator->(); }

			/**
			 * @brief Prefix increment
			 * 
			 * @return iterator& 
			 */
			iterator & operator++()
			{
				// Simple
				if (m_system_config.get_type() == "simple")
				{
					if (m_counter_repetitions < m_system_config.get_repetitions())
					{
						++m_counter_repetitions;
					}
					else
					{
						// Reset repetitions
						m_counter_repetitions = 0;
						// Move to next atom
						++m_it;

						// End of atom list reached
						if (m_it == m_system_config.get_atoms().end())
						{
							// Reset to start of atom list
							m_it = m_system_config.get_atoms().begin();
							// Inc iterations
							++m_counter_iterations;
						}
					}
				}
				// Rotating
				else if (m_system_config.get_type() == "rotating")
				{
					++m_it;
					// If at end of atoms list
					if (m_it == m_system_config.get_atoms().end())
					{
						// Reset to beginning
						m_it = m_system_config.get_atoms().begin();
						// Increment iterations
						++m_counter_iterations;
					}
				}
				
				// Stop when iterations is exceeded
				if (!(m_counter_iterations < m_system_config.get_iterations()))
					m_it = m_system_config.get_atoms().end();

				return *this;
			}

			/**
			 * @brief Postfix increment
			 * 
			 * @return iterator 
			 */
			iterator operator++(int)
			{
				iterator tmp = *this;
				++(*this);
				return tmp;
			}

			/**
			 * @brief Overloaded operator== for iterator
			 * 
			 * @param lhs Left hand side.
			 * @param rhs Right hand side.
			 * @return true if lhs is equivalent to rhs, false otherwise
			 * @return false 
			 */
			friend bool operator==(const iterator & lhs, const iterator & rhs)
			{
				return lhs.m_it == rhs.m_it;
			}

			/**
			 * @brief Overloaded operator!= for iterator
			 * 
			 * @see iterator::operator!=
			 */
			friend bool operator!=(const iterator & lhs, const iterator & rhs)
			{
				return !(operator==(lhs, rhs));
			}

		private:
			unsigned int m_counter_iterations = 0;
			unsigned int m_counter_repetitions = 0;

			std::vector<atom>::iterator m_it;

			system_config & m_system_config;
		};

		/**
		 * @brief Get iterator to first element in the atom sequence
		 * 
		 * @return iterator to first element.
		 */
		iterator begin(void) { return iterator(get_atoms().begin(), *this); }

		/**
		 * @brief Get iterator to atom past the end of the atom sequence
		 * 
		 * Uses nullptr to represent end of sequence.
		 * 
		 * @return iterator to atom past end of atom sequence
		 */
		iterator end(void) { return iterator(get_atoms().end(), *this); }

	private:
		/**
		 * @brief Name of system
		 *
		 */
		std::string m_name;

		/**
		 * @brief Type of system
		 *
		 * Defines how atoms are added to the system. List of valid options
		 * 	- rotating: Atoms are added in rotating order
		 * 	- simple: Groups of the same atoms are added together
		 */
		std::string m_type;

		/**
		 * @brief Number of iterations to repeat adding atoms
		 *
		 */
		unsigned int m_iterations;

		/**
		 * @brief Number of times to repeat adding the same atom
		 * 
		 * Only valid when type is 'simple'
		 */
		unsigned int m_repetitions;

		/**
		 * @brief Vector of atoms
		 * 
		 * Sequence of atoms that are added to the system. How the atoms are
		 * added are controlled with system_config::type,
		 * system_config::iterations, system_config::sequence_len.
		 */
		std::vector<atom> m_atoms;

		/**
		 * @brief Size of system in x direction
		 */
		int m_size_x;

		/**
		 * @brief Size of system in y direction
		 */
		int m_size_y;
	};

	class config
	{
	public:
		/**
		 * @brief Construct a new config object
		 *
		 */
		config(void);

		/**
		 * @brief Construct a new config object and loads a config
		 * 
		 * @param filename Config file to load
		 */
		config(std::string filename);

		/**
		 * @brief Reads a yaml config file
		 *
		 * @param filename Config file.
		 */
		void read_yaml(std::string filename);
	
		std::vector<system_config> system_configs;
	};
}

#endif // SYSTEM_CONFIG_H