#include "system_config.hpp"

atom_builder::system_config::system_config(void) {}

std::ostream & atom_builder::operator<<(std::ostream &os, const system_config & obj)
{
	// Add time
	os << std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()) << "-";

	// Add specifier
	if (obj.m_name.empty())
	{
		os << obj.m_type << "_";
		if (obj.m_type == "rotating")
		{
			os << obj.m_iterations;
		}
		else if (obj.m_type == "simple")
		{
			os << obj.m_iterations << "_" << obj.m_repetitions;
		}
	}
	else
	{
		os << obj.m_name;
	}

	return os;
}

atom_builder::config::config(void) {}

atom_builder::config::config(std::string filename)
{
	read_yaml(filename);
}

void atom_builder::config::read_yaml(std::string filename)
{
	// Load YAML file
	YAML::Node yaml_config = YAML::LoadFile(filename);
	
	YAML::Node systems = yaml_config["systems"];

	// Check if systems is a map
	if (!systems.IsMap())
	{
		// Check if systems exists
		if (!systems.IsDefined())
			throw std::invalid_argument("systems is not defined.");

		throw std::invalid_argument("system is not a map!");
	}

	for (YAML::const_iterator it_system = systems.begin(); it_system != systems.end(); ++it_system)
	{
		atom_builder::system_config tmp;

		// Parse name
		tmp.set_name(it_system->first.as<std::string>());

		if (!it_system->second.IsMap())
			throw std::invalid_argument("system definition is not a map!");	
		YAML::Node system_node = it_system->second;

		// Parse size
		YAML::Node size_node = system_node["size"];
		if (!size_node.IsDefined())
			throw std::invalid_argument("size is not defined!");
		else if (!size_node.IsMap())
			throw std::invalid_argument("size is not a map!");
		else
		{
			// Size x	
			YAML::Node sizex_node = size_node["x"];
			if (!sizex_node.IsDefined())
				throw std::invalid_argument("size.x is not defined!");
			else if (!sizex_node.IsScalar())
				throw std::invalid_argument("size.x is not a scalar!");
			tmp.set_size_x(sizex_node.as<int>());

			// Size y
			YAML::Node sizey_node = size_node["y"];
			if (!sizey_node.IsDefined())
				throw std::invalid_argument("size.y is not defined!");
			else if (!sizey_node.IsScalar())
				throw std::invalid_argument("size.y is not a scalar!");
			tmp.set_size_y(sizey_node.as<int>());
		}

		// Parse type
		YAML::Node type_node = system_node["type"];
		if (!type_node.IsDefined())
			throw std::invalid_argument("type is not defined!");
		else if (!type_node.IsScalar())
			throw std::invalid_argument("type is not a scalar!");
		tmp.set_type(type_node.as<std::string>());

		// Parse iterations
		YAML::Node iterations_node = system_node["iterations"];
		if (!iterations_node.IsDefined())
			throw std::invalid_argument("iterations is not defined!");
		else if (!iterations_node.IsScalar())
			throw std::invalid_argument("iterations is not a scalar!");
		tmp.set_iterations(iterations_node.as<unsigned int>());

		// Parse reptitions
		if (tmp.get_type() == "simple")
		{
			YAML::Node repetitions_node = system_node["repetitions"];
			if (!repetitions_node.IsDefined())
				throw std::invalid_argument("repetitions it not defined!");
			else if (!repetitions_node.IsScalar())
				throw std::invalid_argument("repetitions is not a scalar!");
			tmp.set_repetitions(repetitions_node.as<unsigned int>());
		}

		// Check for atom sequence
		YAML::Node atoms_node = system_node["atoms"];
		if (!atoms_node.IsDefined())
			throw std::invalid_argument("atoms sequence is not defined!");
		else if (!atoms_node.IsSequence())
			throw std::invalid_argument("atoms is not a sequence!");

		// Loop over atom sequence
		for (YAML::const_iterator it_atom = atoms_node.begin(); it_atom != atoms_node.end(); ++it_atom)
		{
			YAML::Node atom_node = *it_atom;

			if (!atom_node.IsScalar())
				throw std::invalid_argument("atom is not a scalar!");

			tmp.get_atoms().push_back(atom(atom_node.as<int>()));	
		}

		system_configs.push_back(tmp);
	}
}