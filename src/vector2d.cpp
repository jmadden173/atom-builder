#include "vector2d.hpp"

atom_builder::vector2d::vector2d(void)
{
    x = 0;
    y = 0;
}

atom_builder::vector2d::vector2d(int _x, int _y)
{
    x = _x;
    y = _y;
}

atom_builder::vector2d & atom_builder::vector2d::operator=(const vector2d &other)
{
    // Self guard
    if (this == &other)
    {
        return *this;
    }

    x = other.x;
    y = other.y;

    return *this;
}


atom_builder::vector2d atom_builder::operator+(const vector2d &lhs, const vector2d &rhs)
{
    vector2d sum;

    sum.x = lhs.x + rhs.x;
    sum.y = lhs.y + rhs.y;

    return sum;
}

bool atom_builder::operator==(const vector2d &lhs, const vector2d &rhs)
{
    // Self guard
    if (&lhs == &rhs)
    {
        return true;
    }

    if ((lhs.x == rhs.x) && (lhs.y == rhs.y))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool atom_builder::operator<(const vector2d &lhs, const vector2d &rhs)
{
    // Check x component
    if (lhs.x < rhs.x)
    {
        return true;
    }
    // Check y if x components are equal
    else if (lhs.x == rhs.x)
    {
        if (lhs.y < rhs.y)
        {
            return true;
        }
    }

    return false;
}