#ifndef SYSTEM_PLOT_HPP
#define SYSTEM_PLOT_HPP

#include <utility>

#include "gnuplot-iostream.h"

#include "atom_system.hpp"

namespace atom_builder
{
    /**
     * Gnuplot object for heatmap plot
     */
    static Gnuplot gp_heatmap;

    /**
     * Gnuplot object for system
     */
    static Gnuplot gp_energy;

	/**
	 * @brief Plot heatmap of the system.
	 *
	 * The "heat" portion of the plot displays the type of atom at each
	 * position in the system.
	 *
	 * @param sys System to plot
	 */
	void plot_heatmap(const atom_system &sys);

	/**
	 * @brief Saves the heatmap plot of the system.
	 * 
	 * @param sys System to plot.
	 * @param filename Filename of image.
	 */
	void save_heatmap(const atom_system & sys, std::string filename);

	/**
	 * @brief Plot linear graph of energy vs number of atoms.
	 *
	 * @param sys System to plot
	 */
	void plot_energy(const atom_system &sys);

	/**
	 * @brief Saves the energy plot of the system.
	 * 
	 * @param sys System to plot.
	 * @param filename Filename of image.
	 */
	void save_energy(const atom_system & sys, std::string filename);

	/**
	 * @brief Get the range of plot in the x axis for the given system.
	 * 
	 * @param sys System
	 * @return Pair of floats in format [low, high]
	 */
	std::pair<float, float> get_xrange(atom_system sys);

	/**
	 * @brief Get the range of plot in the y axis for the given system.
	 * 
	 * @param sys System
	 * @return Pair of floats in format [low, high]
	 */
	std::pair<float, float> get_yrange(atom_system sys);
}

#endif // SYSTEM_PLOT_HPP