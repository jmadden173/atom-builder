#include <string>

#include "atom_system.hpp"
#include "atom.hpp"
#include "system_plot.hpp"
#include "system_config.hpp"

using namespace atom_builder;

const std::string heatmap_ext = "_heatmap.png";
const std::string energy_ext = "_energy.png";

int main(int argc, char *argv[])
{
    config yaml_config(argv[1]);

    std::vector<system_config>::iterator it_config;
    for (it_config = yaml_config.system_configs.begin(); it_config != yaml_config.system_configs.end(); ++it_config)
    {
        // Create new system
        atom_system sys(atom(1), *it_config);

        std::string heatmap_filename = it_config->get_name() + heatmap_ext;
        std::string energy_filename = it_config->get_name() + energy_ext;

        plot_heatmap(sys);
        save_heatmap(sys, heatmap_filename);
        plot_energy(sys);
        save_energy(sys, energy_filename);
    }

    return 0;
}